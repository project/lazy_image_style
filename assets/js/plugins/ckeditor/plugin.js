/**
 * @file
 * Lazy image style CKEditor plugin.
 *
 * Basic plugin inserting abbreviation elements into the CKEditor editing area.
 *
 * @DCG The code is based on an example from CKEditor Plugin SDK tutorial.
 *
 * @see http://docs.ckeditor.com/#!/guide/plugin_sdk_sample_1
 */
(function (Drupal) {

  'use strict';

  CKEDITOR.plugins.add('lazy_image_style', {
    icons: 'lazy-image-style',
    init: function (editor) {
      editor.addCommand('lazyImageStyle', {
        contextSensitive: 1,
        exec: function (editor) {
          let element = editor.getSelection().getSelectedElement();
          // Use only "img" element.
          if (element && element.getName() === 'img') {
            if (element.hasAttribute('data-use-lazy')) {
              element.removeAttribute('data-use-lazy');
              this.setState(CKEDITOR.TRISTATE_OFF);
              return;
            }
            element.setAttribute('data-use-lazy', true);
            this.setState(CKEDITOR.TRISTATE_ON);
          }
        },
        refresh: function (editor, path) {
          let element = path.lastElement;
          if (element.hasAttribute('data-use-lazy')){
            this.setState(CKEDITOR.TRISTATE_ON);
            return;
          }
          this.setState(CKEDITOR.TRISTATE_OFF);
        }
      });
      editor.ui.addButton('lazy-image-style', {
        label: Drupal.t('Use lazy load'),
        command: 'lazyImageStyle',
        toolbar: 'insert'
      });

    }
  });

}(Drupal));
