import { Plugin } from 'ckeditor5/src/core';
import { ButtonView } from 'ckeditor5/src/ui';
import icon from '../../../../../icons/lazy-image-style.svg';
import { Command } from 'ckeditor5/src/core';

/**
 * The base command for "LazyImageStyle" plugin.
 */
class LazyImageStyleCmd extends Command {

  execute() {
    const el = this.editor.model.document.selection.getSelectedElement();

    if (!!el && (el.is('element', 'imageInline') || el.is('element', 'drupalMedia'))) {
      this.editor.model.change(writer => {
        if (el.hasAttribute('data-use-lazy')) {
          writer.removeAttribute('data-use-lazy', el);
        }
        else {
          writer.setAttribute('data-use-lazy', '1', el);
        }
      });
    }
  }

  refresh() {
    const el = this.editor.model.document.selection.getSelectedElement();

    if (!!el) {
      this.isEnabled = (el.is('element', 'imageInline') || el.is('element', 'drupalMedia'));
      this.value = el.hasAttribute('data-use-lazy');
      return;
    }

    this.isEnabled = this.value = false;
  }
}

/**
 * Provides "LazyImageStyle" plugin.
 */
class LazyImageStyle extends Plugin {

  init() {
    const editor = this.editor;
    editor.commands.add(
      'LazyImageStyle',
      new LazyImageStyleCmd(editor),
    );

    // Model to View.
    editor.conversion
      .for('downcast')
      .attributeToAttribute({
        model: 'data-use-lazy',
        view: 'data-use-lazy',
        converterPriority: 'high',
      });

    // View to Model.
    editor.conversion
      .for('upcast')
      .attributeToAttribute({
        model: 'data-use-lazy',
        view: 'data-use-lazy',
        converterPriority: 'low',
      });

    editor.ui.componentFactory.add('lazyImageStyle', (locale) => {
      const button = new ButtonView(locale);

      button.set({
        label: 'lazyImageStyle',
        icon: icon,
        tooltip: true,
        isToggleable: true,
      });

      button.extendTemplate({
        attributes: {
          class: 'button-lazy_image_style',
        },
      });

      this.listenTo(button, 'execute', () =>
        editor.execute('LazyImageStyle'),
      );

      // Button state on command.
      button.bind('isEnabled').to(editor.commands.get('LazyImageStyle'));
      button.bind('isOn').to(editor.commands.get('LazyImageStyle'), 'value');

      return button;
    });
  }

  _addButton(selection) {
    const el = selection.getSelectedElement();
    return (!!el && (el.is('element', 'imageInline') || el.is('element', 'drupalMedia')));
  }

}

class ImageIntegration extends Plugin {
  init() {
    const schema = this.editor.model.schema;

    if (schema.isRegistered('imageInline')) {
      schema.extend('imageInline', {allowAttributes: ['data-use-lazy']});
    }
  }
}

class MediaIntegration extends Plugin {
  init() {
    const schema = this.editor.model.schema;

    if (schema.isRegistered('drupalMedia')) {
      schema.extend('drupalMedia', {allowAttributes: ['data-use-lazy']});
    }
  }
}

export default {
  LazyImageStyle,
  ImageIntegration,
  MediaIntegration
}
